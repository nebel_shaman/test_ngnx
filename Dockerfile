FROM ubuntu:latest

RUN apt-get update \
&&  apt-get install -y wget \
tar \
vim \
gcc \
build-essential \
&& mkdir /home/ngnix \
&& cd  /home/ngnix \
&& wget https://ftp.pcre.org/pub/pcre/pcre-8.44.tar.bz2 \
&& tar -xf pcre-8.44.tar.bz2 \ 
&& cd ./pcre-8.44 \
&& ./configure \
&& cd .. \
&& wget http://zlib.net/zlib-1.2.11.tar.gz \
&& tar -xf zlib-1.2.11.tar.gz \
&& cd ./zlib-1.2.11 \
&& ./configure \
&& cd .. \
&& wget http://www.openssl.org/source/openssl-1.1.1d.tar.gz \
&& tar -zxf openssl-1.1.1d.tar.gz \
&& cd ./openssl-1.1.1d \
&& ./Configure linux-x86_64 --prefix=/usr \
&& cd  .. \
&& wget https://nginx.org/download/nginx-1.18.0.tar.gz \
&& tar -zxf nginx-1.18.0.tar.gz \
&& cd  ./nginx-1.18.0 \
&& ./configure --sbin-path=/sbin/nginx --conf-path=/usr/local/nginx/nginx.conf --pid-path=/usr/local/nginx/nginx.pid --with-pcre=../pcre-8.44 --with-zlib=../zlib-1.2.11 --with-http_ssl_module --with-openssl=../openssl-1.1.1d --with-http_stub_status_module \
&& make \
&& make install

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
